package dbmock

import "gorm.io/gorm"

type RepoPostgres struct {
	db *gorm.DB
}

func (r *RepoPostgres) Create(i *Item) error {
	tx := r.db.Select("message").Create(i)
	return tx.Error
}

func (r *RepoPostgres) Retrieve(id uint) (*Item, error) {
	i := new(Item)
	tx := r.db.First(i, id)
	return i, tx.Error
}

func (r *RepoPostgres) RetrieveByMessage(m string) (*Item, error) {
	i := new(Item)
	tx := r.db.Where("message = ?", m).First(i)
	return i, tx.Error
}

func NewRepoPostgres(db *gorm.DB) *RepoPostgres {
	return &RepoPostgres{db: db}
}
