package dbmock

type Item struct {
	ID      uint `gorm:"primary key;autoIncrement"`
	Message string
}
