package dbmock_test

import (
	"database/sql"
	"dbmock"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"regexp"
	"testing"
)

/*
Given: an Item with "Hello World" as message
When:  creates the item
Then:  retrieves no error
*/
func TestRepoPostgres_Create_with_item_having_a_message(t *testing.T) {
	// Arrange
	sqldb, sqlmck := setUpSqlMock(t)
	gdb := setUpGormMock(t, sqldb)
	repo := setUpRepoPostgres(gdb)

	i := dbmock.Item{
		Message: "Test",
	}

	sqlmck.ExpectBegin()
	sqlmck.
		ExpectQuery(regexp.QuoteMeta(
			`INSERT INTO "items" ("message") VALUES ($1) RETURNING "id"`)).
		WithArgs(i.Message).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).
			AddRow("123456"))
	sqlmck.ExpectCommit()

	// Act
	err := repo.Create(&i)

	// Assert
	assert.NoError(t, err)

}

// INSERT INTO "items" ("message") VALUES ($1) RETURNING "id"
func setUpSqlMock(t *testing.T) (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatal(err)
	}
	return db, mock
}

func setUpGormMock(t *testing.T, db *sql.DB) *gorm.DB {
	gdb, err := gorm.Open(postgres.New(postgres.Config{
		Conn: db,
	}), &gorm.Config{})

	if err != nil {
		t.Fatal(err)
	}

	return gdb
}

func setUpRepoPostgres(db *gorm.DB) *dbmock.RepoPostgres {
	return dbmock.NewRepoPostgres(db)
}
